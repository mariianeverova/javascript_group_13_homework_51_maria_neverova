import { Component } from '@angular/core';

@Component({
  selector: 'app-lottery',
  templateUrl: './lottery.component.html',
  styleUrls: ['./lottery.component.css']
})
export class LotteryComponent {
  constructor() {
    this.getRandomNumber();
  }
  title = 'New Numbers';
  randomNumbersArray: number[]  = [];

  getRandomNumber(): void {
    for(let i = 0; i < 5; i++) {
      let randomNumber = Math.floor(Math.random() * 32) + 5;
      if(this.randomNumbersArray.indexOf(randomNumber) === -1) {
        this.randomNumbersArray[i] = randomNumber;
      }
    }
    this.randomNumbersArray.sort((a: number, b: number) => {
      return a - b;
    })
  }

  onBtnClick() {
    return this.getRandomNumber();
  }


}

