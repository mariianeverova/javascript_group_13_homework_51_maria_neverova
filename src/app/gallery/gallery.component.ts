import { Component } from '@angular/core';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent {
  password = 'homework51';
  imageTitle = '';
  imageUrl = '';
  showForm = false;
  baseImages = [
    {title: 'Steeve', img: 'https://i.pinimg.com/originals/17/44/f8/1744f83a58d812bb9df9cad4fd7f5495.png'},
    {title: 'Thor', img: 'https://i.pinimg.com/originals/71/d8/3e/71d83ee659c1fdc897034547ac5b270c.png'},
    {title: 'Natasha', img: 'https://pbs.twimg.com/media/DbOgzzRUwAEeuZ3.jpg'}
  ];

  onShowForm(event: Event) {
    event.preventDefault();
    const target = <HTMLInputElement>event.target;
    if (this.password === target.value) {
      this.showForm = !this.showForm;
      console.log('ok');
    }
  }

  onAddCard(event: Event) {
    event.preventDefault();
    this.baseImages.push({
      title: this.imageTitle,
      img: this.imageUrl,
    })
    this.reset();
  }

  reset() {
    this.imageTitle = '';
    this.imageUrl = '';
  }

}
